﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePlay : MonoBehaviour
{
    [SerializeField] private int total = 10;

    // set of cards want to use
    [SerializeField] private int cardDeckNumber = 1;
    
    // basic poker constants
    public enum Rank { Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King };
    public enum Suit { Hearts, Spades, Clubs, Diamonds };

    private const int numberOfCardsInDeck = 52;

    // cards position starting position
    Vector3 playerSideStartPosition = new Vector3(151.0f, 213.0f, 0.0f);
    Vector3 dealerSideStartPosition = new Vector3(151.0f, 749.0f, 0.0f);

    public List<Sprite> suitSpriteList = new List<Sprite>();
    
    [SerializeField] public TMP_InputField betAmount;
    [SerializeField] public TextMeshProUGUI totalAmount;
    [SerializeField] public TextMeshProUGUI messageText;

    GameObject dealerHiddenCard;
    private List<Card> totalCards = new List<Card>();
    public Game game = new Game();

    // Start is called before the first frame update
    void Start()
    {
        // load assets
        suitSpriteList.Add(Resources.Load<Sprite>("Sprites/poker-suits-clubs"));
        suitSpriteList.Add(Resources.Load<Sprite>("Sprites/poker-suits-hearts"));
        suitSpriteList.Add(Resources.Load<Sprite>("Sprites/poker-suits-diamonds"));
        suitSpriteList.Add(Resources.Load<Sprite>("Sprites/poker-suits-spades"));

        // disable action buttons
        foreach (GameObject controlButtons in GameObject.FindGameObjectsWithTag("GameController"))
        {
            if (controlButtons.GetComponent<Button>().name == "StartButton") { continue; }
            controlButtons.GetComponent<Button>().interactable = false;
        }

        totalAmount.text = total.ToString();
        betAmount.text = "1";

        generateDecks();

    }

    public List<Card> GetTotalCards()
    {
        return this.totalCards;
    }

    // generate the cards stack based on how many deck of cards want to use
    private void generateDecks()
    {
        for (int num = 0; num < cardDeckNumber; num++)    // number of Deck 
        {
            for (int i = 0; i < 4; i++)          // 4 suits
            {
                for (int j = 0; j < 13; j++)      // 13 ranks
                {
                    totalCards.Add(new Card(i, j));
                }
            }
        }
    }

    public void startNew( int bet, List<Card> totalCards)
    {
        // basic game data setup
        foreach (GameObject gCard in GameObject.FindGameObjectsWithTag("GeneratedCards"))
        {
            Destroy(gCard);
        }
        
        game.setBet(bet);
        game.setTotalCards(totalCards);
        messageText.text = "Started!";

        GameObject.Find("HitButton").GetComponent<Button>().interactable = true;
        GameObject.Find("StandButton").GetComponent<Button>().interactable = true;
        GameObject.Find("DoubleButton").GetComponent<Button>().interactable = true;
        betAmount.interactable = false;
        

        // Dealer got a covered card at first
        Card tempCard = game.ReceiveACard("dealer");
        dealerHiddenCard =  SpawnACard("dealer", 0, true, tempCard.getRank(), tempCard.getSuit());

    }

    public void endTurn(int turnResult, bool isSurrendered)
    {
        
        GameObject.Find("SurrenderButton").GetComponent<Button>().interactable = false;
        GameObject.Find("HitButton").GetComponent<Button>().interactable = false;
        GameObject.Find("StandButton").GetComponent<Button>().interactable = false;
        GameObject.Find("DoubleButton").GetComponent<Button>().interactable = false;

        GameObject.Find("StartButton").GetComponent<Button>().interactable = true;
        betAmount.interactable = true;

        // Reveal dealer's first card
        dealerHiddenCard.transform.GetChild(2).gameObject.GetComponent<Image>().enabled = false;

        // update total based on result and bet
        if (Int32.TryParse(betAmount.text, out int bet))
        {
            if (turnResult == 1)        // player win
            {
                total += bet;
                messageText.text = "You win this round";
            }
            else if (turnResult == 0)   // Round Draw
            {
                messageText.text = "This round draw";
            }
            else if(isSurrendered)      // player surrendered
            {
                total -= (int)Mathf.Ceil( Convert.ToSingle(bet)  / 2);
                messageText.text = "Surrendered! \n You only lost half your bet (Ceiling)";
            }else
            {
                total -= bet;
                messageText.text = "You lost this round! \n And your bet";
            }
            
        }
        else
        {
            Debug.LogError("Invalid bet Amount");
        }

        // if game is over
        if (total <= 0)
        {
            GameObject.Find("ScenesLoad").GetComponent<ScenesLoadController>().LoadGameOverScene();
        }
        else
        {
            totalAmount.text = total.ToString();
        }

        game.ResetGame();

    }

    public GameObject SpawnACard(string side, int position, bool isCovered, int rank, int suit)
    {
        // Spawn a new card
        GameObject cardsDeck = GameObject.Find("cardsDeck");

        string rankToSet = ((Rank) rank).ToString();

        cardsDeck.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = suitSpriteList[suit];

        if( rank != 0 && rank < 10 )
        {
            cardsDeck.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = (rank + 1).ToString();
        }
        else
        {
            cardsDeck.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = rankToSet;
        }

        float offset = 256.0f;
        Vector3 targetPositon = side == "dealer" ? dealerSideStartPosition : playerSideStartPosition;
        targetPositon.x += offset * position; 

        GameObject newCard = Instantiate(cardsDeck, targetPositon, cardsDeck.GetComponent<Transform>().rotation, GameObject.Find("Canvas").GetComponent<Transform>());
        newCard.tag = "GeneratedCards";

        newCard.transform.GetChild(2).gameObject.GetComponent<Image>().enabled = isCovered;

        return newCard;
    }

    // Update is called once per frame
    void Update()
    {
        // TODO create animation to move card from deck to hand
    }

}
