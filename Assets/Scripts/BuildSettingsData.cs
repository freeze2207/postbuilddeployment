﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuildSettingsData", menuName = "My Assets/Build Settings Data")]
public class BuildSettingsData : ScriptableObject
{
    public bool willBuildInstaller;
    public string pathToInno;
    public bool willUploadBuild;
    public bool willUploadInstaller;
    public string pathToButler;
    public string userName;
    public string projectName;
    public string buildChannel;
    public string installerChannel;
}
