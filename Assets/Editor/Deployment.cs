﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

public class Deployment
{
    [PostProcessBuildAttribute(1)]
    public static void OnPostProcessBuild(BuildTarget target, string path_to_target)
    {
        BuildSettingsData buildSettingsData = AssetDatabase.LoadAssetAtPath<BuildSettingsData>("Assets/Editor/BuildSettingsData.asset");
        
        string buildsPath = Application.dataPath + "/../Builds/";

        string cmd = string.Empty;

        // Check flags to generate the command
        if (buildSettingsData.willBuildInstaller)
        {
            cmd += "& \'" + buildSettingsData.pathToInno + "\' \'" + buildsPath + buildSettingsData.projectName + "-installer.iss\'";
            //cmd = "-NoExit -Command " + cmd;

        }

        if(buildSettingsData.willUploadBuild)
        {
            if (cmd != string.Empty)
            {
                cmd += ";& \'" + buildSettingsData.pathToButler + "\' push \'" + buildsPath + buildSettingsData.buildChannel + "\' " + buildSettingsData.userName + "/" + buildSettingsData.projectName + ":" + buildSettingsData.buildChannel;
            }
            else
            {
                cmd += "& \'" + buildSettingsData.pathToButler + "\' push \'" + buildsPath + buildSettingsData.buildChannel + "\' " + buildSettingsData.userName + "/" + buildSettingsData.projectName + ":" + buildSettingsData.buildChannel;
            }
        }

        if(buildSettingsData.willUploadInstaller)
        {
            if(cmd != string.Empty)
            {
                cmd += ";& \'" + buildSettingsData.pathToButler + "\' push \'" + buildsPath + buildSettingsData.projectName + "-installer.exe" + "\' " + buildSettingsData.userName + "/" + buildSettingsData.projectName + ":" + buildSettingsData.buildChannel;
            }
            else
            {
                cmd += "& \'" + buildSettingsData.pathToButler + "\' push \'" + buildsPath + buildSettingsData.projectName + "-installer.exe" + "\' " + buildSettingsData.userName + "/" + buildSettingsData.projectName + ":" + buildSettingsData.buildChannel;
            }
        }

        if (cmd != string.Empty)
        {
            Process process = Process.Start("powershell.exe", cmd);
            process.WaitForExit();
            process.Close();
        }

    }
}
